# hallpass

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Compiles and minifies for stage
```
npm run stage
```
### Compiles and minifies for test
```
npm run test
```
### Compiles and minifies for dev
```
npm run dev
```

### Deploy

After compile for specific env dist directory should be copied to server

DEV: `scp -r dist vps4:/var/www/ehp-front.donatix.info`
TEST: `scp -r dist vps4:/var/www/ehp-test-fe.donatix.info`

where vps4 is ssh to the server
