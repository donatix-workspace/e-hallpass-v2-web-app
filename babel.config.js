module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        useBuiltIns: 'entry',
        corejs: 3
      }
    ]
  ],
  plugins: [
    ['wildcard', {
        'exts': ["js", "es6", "es", "jsx", "javascript","vue"]
    }]
]
}
