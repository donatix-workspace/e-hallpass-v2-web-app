export default {
    data() {
      return {
        reqResponse: {
            type: "",
            message: "",
            errors: {}
        },
      };
    },
    methods: {
        resetResponseMessages() {
          this.reqResponse = {
            type: "",
            message: "",
            errors: {}
          }
        },
        setSuccessResponse(message) {
          this.$v.$reset();
          this.reqResponse = {
            type: 'success',
            message: message ? message : 'Success!',
            errors: {}
          };
        },
        setErrorResponse(message, errors) {
          this.reqResponse = {
            type: 'danger',
            message: message ? message : 'Someting went wrong!',
            errors: errors ? errors : {}
          };
        },
    }
  };
  