export default {
  data() {
    return {
      globalQuery: "",
      filterParams: {
        sort_query: "",
        search_query: ""
      },
      pagination: {
        activePage: 1,
        total: 0,
        pages: 0,
        per_page: { label: "25", value: 25 }
      },
      perPageOptions: [
        { label: "25", value: 25 },
        { label: "50", value: 50 },
        { label: "100", value: 100 },
        { label: "All entries", value: 50 }
      ]
    };
  },
  computed: {
    requestParams() {
      let params = {
        page: this.pagination.activePage,
        per_page: this.pagination.per_page.value
      };

      if (this.filterParams.sort_query) {
        params.sort = this.filterParams.sort_query;
      }
      if (this.filterParams.search_query) {
        params.search_query = this.filterParams.search_query;
      }
      return params;
    }
  },
  mounted() {
    this.getTableData();
  },
  methods: {
    setPerPage(option) {
      if (option) {
        this.pagination.per_page = option;
        this.getTableData();
      }
    },
    searchByQuery(value, type) {
      clearInterval(this.searchTimeOut);
      this.searchTimeOut = setTimeout(() => {
        this.handleQuerySearch(value, type);
      }, 500);
    },
    sortByColumn(column) {
      let columnName = this.columnSearchKeys[column.column]
        ? this.columnSearchKeys[column.column].column[0]
        : column.column;
      this.filterParams.sort_query = `${columnName}:${
        column.asc ? "asc" : "desc"
      }`;
      this.getTableData();
    },
    handleQuerySearch(value, type) {
      let query = value.toString();
      if (
        query.replace(/\s/g, "").length > 0 ||
        query.replace(/\s/g, "").length === 0
      ) {
        this.filterParams.search_query = "";
        if (type && type === "column") {
          this.globalQuery = "";
          for (const key in this.columnSearchKeys) {
            if (Object.hasOwnProperty.call(this.columnSearchKeys, key)) {
              let columnData = this.columnSearchKeys[key];
              if (columnData.value) {
                columnData.column.forEach(col => {
                  this.filterParams.search_query =
                    this.filterParams.search_query +
                    `${col}:"${columnData.value}", `;
                });
              }
            }
          }
          this.filterParams.search_query = this.filterParams.search_query.slice(
            0,
            -2
          );
        } else {
          this.resetSearchQuery();
          this.globalQuery = query;
          this.filterParams.search_query = query ? `"${query}"` : query;
        }

        this.getTableData();
      }
    }
  }
};
