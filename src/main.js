import "core-js/stable";
import Vue from "vue";
import CoreuiVuePro from "../node_modules/@coreui/vue-pro/src/index.js";
import App from "./App";
import router from "./router/index";
import store from "./store/index";
import i18n from "./i18n.js";
import axios from "axios";
import helpers from "./helpers/index";
import {
  iconsSet as icons
} from "./assets/icons/icons.js";
import vSelect from "vue-select";
import {
  library
} from "@fortawesome/fontawesome-svg-core";
import {
  fas
} from "@fortawesome/free-solid-svg-icons";
import {
  FontAwesomeIcon
} from "@fortawesome/vue-fontawesome/";
import CustomTimePicker from "@/components/CustomTimePicker";
import DataTable from "@/components/DataTable";
import datePicker from "vue-bootstrap-datetimepicker";
import vueCustomScrollbar from 'vue-custom-scrollbar'
import "vue-custom-scrollbar/dist/vueScrollbar.css"
import "pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css";
import { datadogRum } from '@datadog/browser-rum'
import {
  version
} from '../package';

Vue.use(datePicker);

library.add(fas);

Vue.component("font-awesome-icon", FontAwesomeIcon);
if (localStorage.getItem("accessToken")) {
  axios.defaults.headers.common["Authorization"] =
    "Bearer " + localStorage.getItem("accessToken");
  // axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';  // store.commit('initialiseStore');
} else {
  if (router.history.current.meta.requiredRoles) {
    // store.dispatch('authentication/unauthorize');
  }
}
axios.defaults.headers.common["Access-Control-Allow-Origin"] = "*";
axios.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    if (error.response.status === 401) {
      store.dispatch("authentication/unauthorize");
      router.push("/login");
      return Promise.reject(error);
    }
    return Promise.reject(error);
  }
);

Vue.component("v-select", vSelect);
Vue.component("cs-time-picker", CustomTimePicker);
Vue.component("vue-scrollbar", vueCustomScrollbar);
Vue.component('DataTable', DataTable);
Vue.use(CoreuiVuePro);

//save Store
store.subscribe((mutation, state) => {
  // localStorage.setItem('store', JSON.stringify(state));
});
Vue.directive('noautofill', {
  // When the bound element is inserted into the DOM...
  bind: function (el) {
    var elem = el.childNodes[1];
    elem.setAttribute('readonly',true);
    elem.setAttribute('autocomplete',"new-password");
    elem.addEventListener('focus',(event)=>{
      event.target.removeAttribute('readonly');
    });
  }
})
Vue.directive('noautofillinput', {
  // When the bound element is inserted into the DOM...
  bind: function (el) {
    el.setAttribute('readonly',true);
    el.setAttribute('autocomplete',"new-password");
    el.addEventListener('focus',(event)=>{
      event.target.removeAttribute('readonly');
    });
  }
})


Vue.prototype.$log = console.log.bind(console);
Vue.prototype.$helpers = helpers;

const init = () => {
  new Vue({
    el: "#app",
    router,
    store,
    icons,
    i18n,
    template: "<App/>",
    components: {
      App
    }
  });
};
//RUM
if (process.env.NODE_ENV == 'production') {

datadogRum.init({
  applicationId: '5af5d426-58bd-4c5d-bd3a-28be4a81b6c9',
  clientToken: 'pubb2923bd9d3a6071966fcf66a6d1ed9c2',
  site: 'datadoghq.com',
  service: 'ehp-v2',
  env: 'production',
  // Specify a version number to identify the deployed version of your application in Datadog
  version: version,
  sampleRate: 1,
  replaySamplerate: 0,
  trackInteractions: true,
  defaultPrivacyLevel: 'mask-user-input'
});

datadogRum.startSessionReplayRecording();
}
// Get user data before init if user is logged in
if (localStorage.getItem("accessToken")) {
  store.dispatch("authentication/getUserDetails").finally(() => {
    store.dispatch("schools/getSchools");
    init();
  });
} else {
  init();
}
