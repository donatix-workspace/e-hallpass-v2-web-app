import helpers from "./helpers";
export default function WebSocketPlugin(socket) {
  return (store) => {
    // called when the store is initialized
    let isSubscribed = false;
    let isMuted = false;
    socket.disconnect();
    store.subscribe((mutation, state) => {
      if (mutation.type == 'authentication/SET_USER' && !isSubscribed) {
        if (state && state.authentication) {
          if (state.authentication.user && state.authentication.user.id) {
            socket.connect();
            var id = state.authentication.user.id;
            var role = state.authentication.user.role.name;
            var schoolId = state.authentication.user.school_id;
            isMuted = !state.authentication.user.audio_enabled;
            if (role == 'student') {
              const studentAppPassChannel = socket.private(`user.appointments.passes.${id}`);
              const studentNowPassChannel = socket.private(`user.passes.${id}`)
              const studentProfileChannel = socket.private(`user.profile.${id}`)
              const favUnavailableChannel = socket.private(`student.unavailables.${schoolId}`)
              let passBlockChannel = socket.private(`pass-blocks.${schoolId}`);
              studentAppPassChannel.listen('AppointmentPassRemind', (payload) => {
                if (state.authentication.user.audio_enabled) {
                  helpers.playNotificationSound();
                }
                store.commit('sockets/SET_APPOINTMENT_PASS_REMIND', payload.appointmentPass);
                store.dispatch('sockets/getNotifications');
                store.commit('appointmentPasses/SOCKET_REFRESH_APP_PASSES', Math.random());
              });

              studentAppPassChannel.listen('AppointmentPassRun', (payload) => {
                if (state.authentication.user.audio_enabled) {
                  helpers.playNotificationSound();
                }
                store.commit('sockets/SET_APPOINTMENT_PASS_RUN', payload.appointmentPass);
                store.dispatch('passes/getStudentActivePass');
                store.commit('sockets/REMOVE_NOTIFICATION', 'appointmentPassRemind');
                store.dispatch('sockets/getNotifications');
                store.commit('appointmentPasses/SOCKET_REFRESH_APP_PASSES', Math.random());
              });
              studentAppPassChannel.listen('AppointmentPassRun', (payload) => {
                if (state.authentication.user.audio_enabled) {
                  helpers.playNotificationSound();
                }
                store.commit('sockets/SET_APPOINTMENT_PASS_RUN', payload.appointmentPass);
                store.dispatch('passes/getStudentActivePass');
                store.commit('sockets/REMOVE_NOTIFICATION', 'appointmentPassRemind');
                store.dispatch('sockets/getNotifications');
                store.commit('appointmentPasses/SOCKET_REFRESH_APP_PASSES', Math.random());
              });
              studentAppPassChannel.listen('AppointmentPassCancelled', (payload) => {
                if (state.authentication.user.audio_enabled) {
                  helpers.playNotificationSound();
                }
                store.dispatch('sockets/getNotifications');
                store.commit('appointmentPasses/SOCKET_REFRESH_APP_PASSES', Math.random());
              });
              studentAppPassChannel.listen('AppointmentPassTimeChanged', (payload) => {
                if (state.authentication.user.audio_enabled) {
                  helpers.playNotificationSound();
                }
                store.dispatch('sockets/getNotifications');
                store.commit('appointmentPasses/SOCKET_REFRESH_APP_PASSES', Math.random());
              });
              studentAppPassChannel.listen('AppointmentPassCreated', (payload) => {
                store.commit('appointmentPasses/SOCKET_REFRESH_APP_PASSES', Math.random());
              });
              studentAppPassChannel.listen('AppointmentPassConfirmed', (payload) => {
                store.commit('appointmentPasses/SOCKET_REFRESH_APP_PASSES', Math.random());
              });
              studentAppPassChannel.listen('AppointmentPassAcknowledged', (payload) => {
                store.commit('appointmentPasses/SOCKET_REFRESH_APP_PASSES', Math.random());
              });
              studentAppPassChannel.listen('AppointmentPassComment', (payload) => {
                store.commit('appointmentPasses/SOCKET_REFRESH_APP_PASSES', Math.random());
              });

              studentNowPassChannel.listen('PassApproved', (payload) => {
                if (!isMuted && payload.notifyStudent) {
                  helpers.playNotificationSound();
                }
                store.dispatch('passes/getStudentActivePass').then(() => {
                  if (window.vueRouter.currentRoute.path !== '/passes/activepass') {
                    window.vueRouter.push('/passes/activepass');
                  }
                  store.dispatch('passes/getStudentPassHistoryAsStudent');
                })
              });
              studentNowPassChannel.listen('PassCanceled', (payload) => {
                store.dispatch('passes/getStudentActivePass').then(() => {
                  if (window.vueRouter.currentRoute.path !== '/passes/activepass') {
                    window.vueRouter.push('/passes/activepass');
                  }
                })
              });
              studentNowPassChannel.listen('PassCreated', (payload) => {
                if(payload.notifyStudent && !isMuted){
                  helpers.playNotificationSound();
                }
                store.dispatch('passes/getStudentActivePass').then(() => {
                  if (window.vueRouter.currentRoute.path !== '/passes/activepass') {
                    window.vueRouter.push('/passes/activepass');
                  }
                  store.dispatch('passes/getStudentPassHistoryAsStudent');
                })
              });

              studentNowPassChannel.listen('PassExpired', (payload) => {
                store.dispatch('passes/getStudentActivePass').then(() => {

                  if (window.vueRouter.currentRoute.path !== '/passes/activepass') {
                    window.vueRouter.push('/passes/activepass');
                  }

                })
              });

              studentNowPassChannel.listen('PassArrived', (payload) => {
                store.dispatch('passes/getStudentActivePass').then(() => {

                  if (window.vueRouter.currentRoute.path !== '/passes/activepass') {
                    window.vueRouter.push('/passes/activepass');
                  }

                });
              });
              studentNowPassChannel.listen('PassEnded', (payload) => {
                store.dispatch('passes/getStudentActivePass').then(() => {

                  if (window.vueRouter.currentRoute.path !== '/passes/activepass') {
                    window.vueRouter.push('/passes/activepass');
                  }

                });
              });
              studentNowPassChannel.listen('PassInOut', (payload) => {
                store.dispatch('passes/getStudentActivePass').then(() => {

                  if (window.vueRouter.currentRoute.path !== '/passes/activepass') {
                    window.vueRouter.push('/passes/activepass');
                  }

                });

              });
              studentNowPassChannel.listen('PassReturning', (payload) => {
                store.dispatch('passes/getStudentActivePass').then(() => {
                  if (window.vueRouter.currentRoute.path !== '/passes/activepass') {
                    window.vueRouter.push('/passes/activepass');
                  }
                });
              });
              studentNowPassChannel.listen('PassCommentCreatedEvent', (payload) => {
                if (state.authentication.user.audio_enabled) {
                  helpers.playNotificationSound();
                }
                store.dispatch('passes/getStudentActivePass').then(() => {
                  if (window.vueRouter.currentRoute.path !== '/passes/activepass') {
                    window.vueRouter.push('/passes/activepass');
                  }
                });
              });

              studentNowPassChannel.listen('PinStatusEvent', (payload) => {
                const pinAttempts = payload.pinAttempts;
                if (pinAttempts === 0) {
                  store.commit("authentication/RESET_PIN_ATTEMPTS_COUNT");
                } else if (pinAttempts === 3) {
                  store.dispatch('authentication/logOut').then(() => {
                    window.location.reload();
                  });
                } else if (pinAttempts === 5) {
                  store.commit("authentication/SET_USER_IS_LOCKED", true);
                  window.vueRouter.push('/locked');
                }
              });

              studentProfileChannel.listen('UserUnlockedEvent', (payload) => {
                store.commit("authentication/SET_USER_IS_LOCKED", false);
                store.commit("authentication/RESET_PIN_ATTEMPTS_COUNT");
                window.vueRouter.push('/passes/activepass');
              });


              passBlockChannel.listen('PassBlockRunnedEvent', (payload) => {
                store.commit('passBlocks/SET_ACTIVE_PASS_BLOCK', payload.passBlock);
              });

              favUnavailableChannel.listen('FavoriteUnavailableEvent', (payload) => {
                if(payload.forFavorite && payload.forFavorite.type === "App\\Models\\User"){
                  store.commit('favorites/UPDATE_FAVORITE_UNAVAILABILITY', payload.forFavorite);
                }
              });
            }else{
              let unavailablesChannel = socket.private(`user.unavailables.${id}`);

              unavailablesChannel.listen('UnavailableUpdate', (payload) => {
                store.commit('authentication/SET_USER_UNAVAILABLE', payload.unavailableStatus);
                store.commit('dashboard/SET_UNAVAILABLE', payload.unavailable);
              });

              unavailablesChannel.listen('UnavailableExpired', (payload) => {
                store.commit('authentication/SET_USER_UNAVAILABLE', null);
                store.commit('dashboard/SET_UNAVAILABLE', null);
              });
            }
            let passBlockChannel = socket.private(`pass-blocks.${schoolId}`);
            let modulesChannel = socket.private(`modules.${schoolId}`);
            passBlockChannel.listen('PassBlockCreated', (payload) => {
              store.commit('passBlocks/PUSH_PASS_BLOCK', payload.passBlock);
            });
            passBlockChannel.listen('PassBlockRemoved', (payload) => {
              store.commit('passBlocks/DELETE_PASS_BLOCK', payload.passBlockId);
            });
            modulesChannel.listen('ModuleChanged', (payload) => {
              store.dispatch("schools/getSchools");
            });

            isSubscribed = true;
          }else{
          }
        }
      }
      if(state.authentication && mutation.type == 'authentication/TOGGLE_USER_AUDIO'){
        isMuted = !state.authentication.user.audio_enabled;
      }
    });
  }
}
