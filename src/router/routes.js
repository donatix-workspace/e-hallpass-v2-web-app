import * as Views from '../views/';
import SideBarLayout from '@/layouts/SideBarLayout.vue'

const Routes = [
  {
    path: '/',
    name: 'Home',
    redirect: '/dashboard',
    component: SideBarLayout,
    meta: {
      requiredRoles: ['superadmin', 'admin', 'teacher', 'staff'],
    },
    children: [
      {
        path: '/dashboard',
        name: 'Dashboard',
        component: Views.Dashboard,
        meta: {
          requiredRoles: ['superadmin', 'admin', 'teacher', 'staff', 'student'],
          classes: 'dashboard-view',
        }
      },
      {
        path: '/pass-history',
        name: 'Pass History',
        component: Views.PassHistory,
        meta: {
          requiredRoles: ['superadmin', 'admin', 'teacher', 'staff'],
        }
      },
      {
        path: '/teacher-pass',
        name: 'Teacher Pass',
        component: Views.TeacherPass,
        meta: {
          requiredRoles: ['superadmin', 'admin', 'teacher', 'staff'],
        }
      },
      {
        path: '/appointment-pass',
        name: 'Appointment Pass',
        component: Views.AppointmentPass,
        meta: {
          requiredRoles: ['superadmin', 'admin', 'teacher', 'staff', 'student'],
          requiredModule: 'Appointmentpass'
        }
      },
      {
        path: '/out-of-office',
        name: 'Out of office',
        component: Views.OutOfOffice,
        meta: {
          requiredRoles: ['superadmin', 'admin', 'teacher', 'staff'],
        }
      },

      {
        path: '/blocking',
        name: 'Pass Limit / Blocking',
        component: {
          render(c) {
            return c('router-view')
          }
        },
        redirect: '/blocking/limit-student-pass',
        meta: {
          requiredRoles: ['superadmin', 'admin'],
        },
        children: [
          {
            path: '/blocking/limit-student-pass',
            name: 'Limit Student Pass',
            component: Views.LimitStudentPass,
            meta: {
              requiredRoles: ['superadmin', 'admin'],
            }
          },
          {
            path: '/blocking/limit-location-max-cap',
            name: 'Limit Location Max Cap',
            component: Views.LimitLocationMaxCap,
            meta: {
              requiredRoles: ['superadmin', 'admin', 'staff'],
            }
          },
          {
            path: '/blocking/limit-active-pass',
            name: 'Limit Active Pass',
            component: Views.LimitActivePasses,
            meta: {
              requiredRoles: ['superadmin', 'admin'],
            }
          },
          {
            path: '/blocking/limit-location-availability',
            name: 'Limit Location Availability',
            component: Views.LimitLocationAvailability,
            meta: {
              requiredRoles: ['superadmin', 'admin'],
            }
          },
          {
            path: '/blocking/ab-polarity',
            name: 'A/B Polarity',
            component: Views.ABPolarity,
            meta: {
              requiredRoles: ['superadmin', 'admin', 'teacher', 'staff'],
            }
          },
          {
            path: '/blocking/pass-blocking',
            name: 'Pass Blockingy',
            component: Views.PassBlocking,
            meta: {
              requiredRoles: ['superadmin', 'admin'],
            }
          },
        ]
      },
      {
        path: '/pass-settings',
        name: 'Pass Settings',
        redirect: '/pass-settings/rooms',
        component: {
          render(c) {
            return c('router-view')
          }
        },
        meta: {
          requiredRoles: ['superadmin', 'admin'],
        },
        children: [
          {
            path: '/pass-settings/rooms',
            name: 'Rooms',
            component: Views.Rooms,
            meta: {
              requiredRoles: ['superadmin', 'admin'],
            }
          },
        ]
      },
      {
        path: '/user-managment',
        name: 'User Managment',
        redirect: '/user-managment/main-users',
        component: {
          render(c) {
            return c('router-view')
          }
        },
        meta: {
          requiredRoles: ['superadmin', 'admin'],
        },
        children: [
          {
            path: '/user-management/main-users',
            name: 'Main Users',
            component: Views.Users,
            meta: {
              requiredRoles: ['superadmin', 'admin'],
            }
          }
        ]
      },
      {
        path: '/user-management/archived-users',
        name: 'Archived Users',
        component: Views.ArchivedUsers,
        meta: {
          requiredRoles: ['superadmin', 'admin', 'teacher', 'staff', 'student'],
        }
      },
      {
        path: '/user-management/substitute-users',
        name: 'Substitute Users',
        component: Views.SubstituteUsers,
        meta: {
          requiredRoles: ['superadmin', 'admin', 'teacher', 'staff', 'student'],
        }
      },
      {
        path: '/user-settings',
        name: 'User Settings',
        component: Views.UserSettings,
        meta: {
          requiredRoles: ['superadmin', 'admin', 'teacher', 'staff', 'student'],
        }
      },
      {
        path: '/passes/create',
        name: 'Create Pass',
        component: Views.StudentPass,
        meta: {
          requiredRoles: ['superadmin', 'admin', 'teacher', 'staff', 'student'],
        }
      },
      {
        path: '/passes/activepass',
        name: 'Active Pass',
        component: Views.ActivePass,
        meta: {
          requiredRoles: ['superadmin', 'admin', 'teacher', 'staff', 'student'],
        }
      },
      {
        path: '/training',
        name: 'Training',
        redirect: '/training/video-tutorial',
        component: {
          render(c) {
            return c('router-view')
          }
        },
        meta: {
          requiredRoles: ['superadmin', 'admin', 'teacher', 'staff', 'student'],
        },
        children: [
          {
            path: '/training/documents',
            name: 'Documents',
            component: Views.TrainingDocuments,
            meta: {
              requiredRoles: ['superadmin', 'admin', 'teacher', 'staff', 'student'],
            }
          },
          {
            path: '/training/videos',
            name: 'Training Videos',
            component: Views.VideoTraining,
            meta: {
              requiredRoles: ['superadmin', 'admin', 'teacher', 'staff', 'student'],
            }
          },
          // {
          //   path: '/training/admin-guide',
          //   name: 'Video Tutorial',
          //   component: Views.AdminGuide,
          //   meta: {
          //     requiredRoles: ['superadmin', 'admin'],
          //   }
          // },
          // {
          //   path: '/training/teacher-guide',
          //   name: 'Video Tutorial',
          //   component: Views.TeacherGuide,
          //   meta: {
          //     requiredRoles: ['superadmin', 'admin', 'teacher'],
          //   }
          // },
          // {
          //   path: '/training/video-tutorials',
          //   name: 'Video Tutorials',
          //   component: Views.VideoTraining,
          //   meta: {
          //     requiredRoles: ['superadmin', 'admin', 'teacher', 'staff', 'student'],
          //   }
          // },
          // {
          //   path: '/training/quick-reference',
          //   name: 'Quick References',
          //   component: Views.QuickReference,
          //   meta: {
          //     requiredRoles: ['superadmin', 'admin', 'teacher', 'staff', 'student'],
          //   }
          // },
        ]
      },
      {
        path: '/new-releases',
        name: 'New Releases',
        component: Views.NewReleases,
        meta: {
          requiredRoles: ['superadmin', 'admin', 'teacher', 'staff', 'student'],
        }
      },
      {
        path: '/kiosks',
        name: 'Kiosks',
        component: Views.Kiosks,
        meta: {
          requiredRoles: ['superadmin', 'admin', 'teacher', 'staff', 'student'],
          requiredModule: 'Kiosk',
          classes: 'kiosks-view',
        },
      },
      {
        path: '/help-desk-portal',
        name: 'Help Desk Portal',
        component: Views.HelpDeskPortal,
        meta: {
          requiredRoles: ['superadmin', 'admin'],
        }
      },
      {
        path: '/admin-reports',
        name: 'Admin Reports',
        redirect: '/admin-reports/summary-reports',
        component: {
          render(c) {
            return c('router-view')
          }
        },
        meta: {
          requiredRoles: ['superadmin', 'admin'],
        },
        children: [
          {
            path: '/admin-reports/summary-reports',
            name: 'Summary Reports',
            component: Views.SummaryReports,
            meta: {
              requiredRoles: ['superadmin', 'admin'],
            }
          },
          {
            path: '/admin-reports/contact-tracing',
            name: 'Contact Tracing',
            component: Views.ContactTracing,
            meta: {
              requiredRoles: ['superadmin', 'admin'],
            }
          },
        ]
      },
      {
        path: '/pass-settings/pass-transparency',
        name: 'Pass Transparency',
        component: Views.PassTransparency,
        meta: {
          requiredRoles: ['superadmin', 'admin'],
        }
      },
      {
        path: '/pass-settings/auto-pass',
        name: 'Auto Pass',
        component: Views.AutoPass,
        meta: {
          requiredRoles: ['superadmin', 'admin'],
        }
      },
      {
        path: '/pass-settings/pass-times',
        name: 'Pass Times',
        component: Views.PassTimes,
        meta: {
          requiredRoles: ['superadmin', 'admin'],
        }
      },
      {
        path: '/modules',
        name: 'Modules List',
        component: Views.ModulesList,
        meta: {
          requiredRoles: ['superadmin', 'admin'],
        }
      },
      {
        path: '/file-uploads',
        name: 'File Uploads',
        component: Views.FileUploads,
        meta: {
          requiredRoles: ['superadmin', 'admin'],
        }
      },
      {
        path: '/pass-settings/periods',
        name: 'Periods',
        component: Views.Periods,
        meta: {
          requiredRoles: ['superadmin', 'admin'],
        }
      }
    ]
  },
  // {
  //   path: '/login',
  //   name: 'Login',
  //   component: Views.Login,
  // },
  {
    path: '/login/:source?/:type?',
    name: 'Login',
    component: Views.Login,
  },
  {
    path: '/glogin',
    name: 'glogin',
    component: Views.Login,
  },
  {
    path: '/ologin',
    name: 'ologin',
    component: Views.Login,
  },
  {
    path: '/clogin',
    name: 'clogin',
    component: Views.Login,
  },
  {
    path: '/classlinklogin',
    name: 'classlinklogin',
    component: Views.Login,
  },
  {
    path: '/classlinkoauth',
    name: 'classlinkoauth',
    redirect: '/login/web/classlink'
  },
  {
    path: '/clever/oauth',
    name: 'cleveroauth',
    redirect: '/login/web/clever'
  },
  {
    path: '/gg4l',
    name: 'gg4l',
    component: Views.Login,
  },
  {
    path: '/forgotten-password',
    name: 'Forgotten Password',
    component: Views.ForgottenPassword
  },
  {
    path: '/kiosk/login',
    name: 'Kiosk Login',
    component: Views.KioskLogin
  },
  {
    path: '/kiosk/launch',
    name: 'Kiosk System',
    component: Views.KioskLaunch,
  },
  {
    path: '/locked',
    name: 'locked',
    component: Views.Locked
  },
  {
    path: '/acknowledged-pass',
    name: 'locked',
    component: Views.AcknowledgedPass
  },
  {
    path: '/404',
    name: '404',
    component: Views.NotFound
  },
  {
    path: '*',
    name: '404',
    redirect: '/404',
  }
];

export default Routes;
