import Vue from 'vue'
import Router from 'vue-router'
import Routes from './routes'
import store from '@/store/index'
import {
  cibWindows
} from '@coreui/icons'

Vue.use(Router)

let router = new Router({
  mode: 'history', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'open active',
  scrollBehavior: () => ({
    y: 0
  }),
  routes: [
    ...Routes
  ]
})
// Expose the router to be used by sockets.
window.vueRouter = router;

// nav guard for authenticated users
let currentRole = store.getters['authentication/getUserRole'];
let user = store.getters['authentication/user'];
let activeModules = null;


router.beforeEach((to, from, next) => {

  if (to.matched.some(route => route.meta.requiredRoles)) { // check if 'to' route requires spec ROLE
    activeModules = store.getters['schools/activeModulesNames'];
    if (!to.meta.requiredModule || activeModules.includes(to.meta.requiredModule)) { // check for required module and chekc if is activated
      if (localStorage.getItem('accessToken')) { // check if 'user' is auth
      if (!currentRole) { // check if user has setted ROLE
        user = store.getters['authentication/user'];
        if (!user.is_locked) {
          store.dispatch('passes/getStudentActivePass').then(() => {
            currentRole = store.getters['authentication/getUserRole'];
            if (to.meta.requiredRoles.includes(currentRole)) {
              if (currentRole == 'student' && (to.path == '/' || to.path == '/dashboard')) {

                let activePass = store.getters['passes/activePass'];
                if (currentRole == 'student' && (activePass && activePass.pass_status !== 0) && to.path != '/passes/activepass') {
                  return next('/passes/activepass');
                } else {
                  return next('/passes/create');
                }
              } else {

                let activePass = store.getters['passes/activePass'];
                if (currentRole == 'student' && (activePass && activePass.pass_status !== 0) && to.path != '/passes/activepass') {
                  return next('/passes/activepass');
                } else {
                  return next();
                }
              }
            } else {
              return next('/404');
            }
          });
        } else {
          return next('/locked');
        }
      } else {
        if (to.meta.requiredRoles.includes(currentRole)) {
          if (currentRole == 'student') {
            if (to.path == '/' || to.path == '/dashboard') {
              return next('/passes/create');
            }
            let activePass = store.getters['passes/activePass'];
            if (currentRole == 'student' && activePass && activePass.pass_status !== 0 && to.path != '/passes/activepass') {
              return next('/passes/activepass');
            } else {
              return next();
            }

          } else {
            return next();
          }
        } else {
          return next('/404');
        }
      }
    } else {
      return next('/login');
    }
  }else {
    return next('/404');
  }


  } else {
    next()
  }
});


export default router;
