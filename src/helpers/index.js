import moment from "moment-timezone";

const helpers = {
  currTzDateTime: (date, time, format = "YYYY-MM-DD HH:mm") => {
    let newDate = new Date(date).toLocaleDateString("en-GB") + " " + time;
    newDate = moment(newDate, "DD-MM-YYYY HH:mm");
    return helpers.transformDateTime(newDate, format);
  },
  currTzDate: (date, format = "MM/DD/YYYY") => {
    let newDate = new Date(date).toLocaleDateString("en-GB");
    newDate = moment(newDate, "DD-MM-YYYY");
    return helpers.transformDate(newDate, format);
  },
  transformUTC(date, format='MM/DD/YYYY hh:mm A'){
    return moment.utc(date).tz(moment().tz()).format(format)
  },
  transformDateTime: (date, format, zone) => {
    let _date = moment(date).format(format ? format : "MM/DD/YYYY HH:mm");
    let _zone = zone ? zone : " " + moment().tz();
    return _date + _zone;
  },
  getHTMLDateTime: (date, formatDate = "MM/DD/YYYY", formatTime = "HH:mm", separator = "&nbsp;&nbsp;") => { //readonly
    let _date = moment(date).format(formatDate);
    let _time = moment(date).format(formatTime);
    return _date + separator + _time;
  },
  transformDate: (date, format) => {
    return moment(date).format(format ? format : "MM/DD/YYYY");
  },
  addMinutesToTime: (time, minutes, fromformat, toFormat) => {
    return moment(time, fromformat ? fromformat : "HH:mm")
      .add(minutes, "minutes")
      .format(toFormat ? toFormat : "HH:mm");
  },
  removeMinutesFromTime: (time, minutes) => {
    return moment(time, "HH:mm")
      .subtract(minutes, "minutes")
      .format("HH:mm");
  },
  formatDate(date, formatFrom, formatТо) {
    return formatFrom
      ? moment(date, formatFrom).format(formatТо)
      : moment(date).format(formatТо);
  },
  date(date) {
    return date ? moment(date) : moment();
  },
  getEndOfTheDay() {
    return moment().format("YYYY-MM-DD 23:59:59");
  },
  secondsToTimeString(SECONDS) {
    return new Date(SECONDS * 1000).toISOString().substr(11, 8);
  },
  playNotificationSound() {
    const notificationsAudio = document.getElementById("notifications-audio");
    notificationsAudio.currentTime = 0;
    notificationsAudio.muted = true; //Chrome problem fix
    notificationsAudio.play();
    notificationsAudio.muted = false; //Chrome problem fix
  },
  stopNotificationSound() {
    const notificationsAudio = document.getElementById("notifications-audio");
    notificationsAudio.pause();
    notificationsAudio.currentTime = 0;
  },
  scrollToTop(toTop=0){
    var container = document.getElementsByClassName("main-scroll-container")[0];
    if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1 || navigator.userAgent.toLowerCase().indexOf('safari') > -1){
      container.classList.add('scrollTopFix');
      setTimeout(() => {
        container.classList.remove('scrollTopFix');
      }, 700);
    }
    setTimeout(() => {
      container.scrollTo({top: toTop, behavior: 'smooth'});
    }, 200);
  },
  setModalToggleClass(value){
    let body = document.getElementsByTagName('body');
    if(value){
      body[0].classList.add('modal-opened');
    }else{
      body[0].classList.remove('modal-opened');
    }
  }
};

export default helpers;
