import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';
//Set Base API URL
axios.defaults.baseURL = process.env.VUE_APP_API;

import layout from './modules/layout';
import authentication from './modules/authentication';
import dashboard from './modules/dashboard';
import rooms from './modules/rooms';
import users from './modules/users';
import schools from './modules/schools';
import appointmentPasses from './modules/appointmentPasses';
import outOfOfficePasses from './modules/outOfOfficePasses';
import proxyPasses from './modules/proxyPasses';
import activePass from './modules/activePass';
import comments from './modules/comments';
import helpCenter from './modules/helpCenter';
import polarities from './modules/polarities';
import passes from './modules/passes';
import periods from './modules/periods';
import favorites from './modules/favorites';
import kiosks from "./modules/kiosks";
import sockets from "./modules/sockets";
import releases from './modules/releases';
import Echo from "laravel-echo";
import VueEcho from "vue-echo-laravel";
import WebSocketPlugin from "../WebSocketPlugin";
import modules from "./modules/modules";
import contactTracing from "./modules/contactTracing";
import usersImport from "./modules/usersImport";
import passLimits from "./modules/passLimits";
import passBlocks from "./modules/passBlocks";
import summaryReports from "./modules/summaryReports";
import search from "./modules/search";
import files from "./modules/files";

window.io = require('socket.io-client');
export const EchoInstance = new Echo({
  broadcaster: 'socket.io',
  host: process.env.VUE_APP_WS_HOST,
  encrypted: (process.env.VUE_APP_WS_ENCRYPTED === 'true'),
  auth: {
    headers: {
      'Authorization': 'Bearer ' + localStorage.getItem('accessToken'),
    }
  },
  transports: ['websocket'],
});
Vue.use(VueEcho, EchoInstance);
const websocket = WebSocketPlugin(EchoInstance);

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  modules: {
    layout,
    authentication,
    dashboard,
    rooms,
    users,
    schools,
    appointmentPasses,
    outOfOfficePasses,
    proxyPasses,
    polarities,
    passes,
    periods,
    activePass,
    comments,
    helpCenter,
    favorites,
    kiosks,
    sockets,
    releases,
    modules,
    contactTracing,
    usersImport,
    passLimits,
    passBlocks,
    summaryReports,
    search,
    files
  },
  mutations: {
    initialiseStore(state) {
      // Get the store form local storage to always have initial data.
      if (localStorage.getItem('store')) {
        // Replace the state object with the stored item
        this.replaceState(
          Object.assign(state, JSON.parse(localStorage.getItem('store')))
        );
      }

    }
  },
  strict: debug,
  plugins: [websocket],
});
