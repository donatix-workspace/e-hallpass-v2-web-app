import axios from 'axios';

const state = {
};

const getters = {
};

const mutations = {
};

const actions = {
    createComment(context, comment){
        return new Promise((resolve, reject) => {
            axios.post(`/comments/${comment.toID}`, comment.data)
            .then((response) => {
                resolve(response);
            })
            .catch((err) => {
                reject(err);
            });
        })
    }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
