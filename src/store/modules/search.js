import axios from 'axios';

const state = {};

const getters = {};

const mutations = {};

const actions = {
  searchByTypeAndQuery(context, params) {
    return new Promise((resolve, reject) => {
      axios.get(`/search`, {
        params,
      })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
