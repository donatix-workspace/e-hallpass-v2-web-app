import axios from 'axios';
import {
  Promise
} from 'core-js';

const state = {
  passes: null,
  periods: null,
  passHistories: null,
  pass_limit: {},
  app_pass_limit: {},
  activePass: null,
  passesTimeSetting: null,
  transparencies: null,
  transparencyStatus: false,
  studentPassHistories: null
};

const mutations = {
  SET_PASSES(state, passesCollection) {
    state.passes = passesCollection;
  },
  SET_PERIODS(state, periodsCollection) {
    state.periods = periodsCollection;
  },
  SET_PASS_HISTORIES(state, passCollection) {
    state.passHistories = passCollection;
  },
  PUSH_PASS_HISTORIES(state, passesCollection){
    if(state.passHistories && state.passHistories.length){
      state.passHistories = state.passHistories.concat(passesCollection);
    }else{
      state.passHistories = passesCollection;
    }
  },
  UPDATE_PASS_HISTORY_COMMENTS(state, passObj){
    if(state.passHistories){
      let newCollection = state.passHistories.map((pass) => {
        if(pass.id === passObj.id){
          pass.comments = passObj.comments;
        }
        return pass;
      });
      state.passHistories = newCollection;
    }
  },
  SET_PASSES_TIME_SETTING(state, timeSetting) {
    state.passesTimeSetting = timeSetting;
  },
  SET_PASS_LIMIT(state, pass_limit) {
    state.pass_limit = pass_limit;
  },
  SET_APP_PASS_LIMIT(state, app_pass_limit) {
    state.app_pass_limit = app_pass_limit;
  },
  SET_ACTIVE_PASS(state, activePass) {
    state.activePass = activePass;
  },
  SET_TRANSPARENCIES(state, transparenciesCollection) {
    state.transparencies = transparenciesCollection;
  },
  ADD_TRANSPARENCY(state, newTransparency) {
    state.transparencies.transparency_users.data.push(newTransparency);
  },
  REMOVE_TRANSPARENCY(state, transparency) {
    state.transparencies.transparency_users.data = state.transparencies.transparency_users.data.filter((oldTransparency) => oldTransparency.user_id !== transparency.user_id);
  },
  SET_TRANSPARENCY_STATUS(state, newStatus) {
    state.transparencyStatus = newStatus;
  },
  UPDATE_PASS_HISTORY(state, passObj){
    let newCollection = state.passHistories.map((pass) => {
      if(pass.id === passObj.id){
        return passObj;
      }
      return pass;
    });
    state.passHistories = newCollection;
  },
  SET_STUDENT_PASS_HISTORIES(state, passHistories){
    state.studentPassHistories = passHistories;
  }
};


const getters = {
  passes(state) {
    return state.passes;
  },
  formListPeriods(state) {
    if (state.periods) {
      return state.periods.map(period => {
        return {
          value: [period.id],
          label: period.name,
          attrs: {}
        }
      });
    }
  },
  formSelectTransparencyUsers(state) {
    if (state.transparencies) {
      return state.transparencies.users.map(user => {
        return {value: user.id, label: user.first_name + ' ' + user.last_name};
      });
    }
  },
  passHistories(state) {
    return state.passHistories;
  },
  passesTimeSettings(state) {
    return state.passesTimeSetting;
  },
  pass_limit(state) {
    return state.pass_limit
  },
  app_pass_limit(state) {
    return state.app_pass_limit
  },
  activePass(state) {
    return state.activePass;
  },
  transparencies(state) {
    return state.transparencies;
  },
  transparencyUserIDs(state) {
    return state.transparencies.map((user) => {return user.user_id});
  },
  transparencyStatus(state) {
    return state.transparencyStatus;
  },
  studentPassHistories(state){
    return state.studentPassHistories;
  }
};

const actions = {
  getPassesTimeSetting(context) {
    return new Promise((resolve, reject) => {
      axios.get("/admin/passes/time")
        .then(response => {
          let data = response.data.data;
          if (data) {
            context.commit('SET_PASSES_TIME_SETTING', data.time_settings);
            context.commit('polarities/SET_POLARITY_MESSAGE', data.time_settings.polarity_message , {root: true});
          }
          resolve(response);
        })
        .catch(error => {
          console.log(error);
          reject(error);
        })
    });
  },
  setPassesTimeSetting(context, form) {
    return new Promise((resolve, reject) => {
      axios.put("/admin/passes/time", form)
        .then(response => {
          context.dispatch('getPassesTimeSetting');
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    })
  },
  getPassHistories(context, filterData) {
    return new Promise((resolve, reject) => {
      axios.get("/admin/passes-histories/filter", {
        params: filterData
      },)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          console.log(error);
          reject(error);
        })
    });
  },
  getCSVPassHistoriesExport(context, filterData) {
    return new Promise((resolve, reject) => {
      axios.get("/admin/passes-histories/exports/csv", {
        params: filterData
      })
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  getPasses(context) {
    return new Promise((resolve, reject) => {
      axios.get(`/passes`)
        .then((response) => {
          resolve(response);
          context.commit('rooms/SET_ROOMS', response.data.data.rooms, {
            root: true
          });
          context.commit("users/SET_USERS", response.data.data.users, {
            root: true
          });
          context.commit("SET_PERIODS", response.data.data.periods);
          context.commit("SET_PASSES", response.data.data.passes);
          context.commit("SET_PASS_LIMIT", response.data.data.pass_limit);
          context.commit("SET_APP_PASS_LIMIT", response.data.data.appointment_passes);
        })
        .catch((err) => {
          reject(err);
        })
    })
  },
  getTransparencies(context, filterData) {
    return new Promise((resolve, reject) => {
      axios.get(`/admin/transparencies`, {
        params: filterData
      })
        .then((response) => {
          let data = response.data.data;
          context.commit("SET_TRANSPARENCIES", data.transparency_users.data)
          context.commit("SET_TRANSPARENCY_STATUS", data.transparency_flags.exists)
          resolve(response);
        }).catch((err) => {
        reject(err);
      })
    })
  },
  createTransparency(context, data) {
    return new Promise((resolve, reject) => {
      axios.post(`/admin/transparencies`, data)
        .then(response => {
          resolve(response);
        }).catch(error => {
        console.log(error);
        reject(error);
      })
    })
  },
  toggleTransparency(context, data) {
    return new Promise((resolve, reject) => {
      axios.put(`/admin/transparencies/toggle`, data).then(response => {
        context.commit('SET_TRANSPARENCY_STATUS', data.status);
        resolve(response);
      }).catch((err) => {
        reject(err);
      })
    })
  },
  deleteTransparency(context, data) {
    return new Promise((resolve, reject) => {
      axios.delete(`/admin/transparencies/${data.transparency_id}`)
        .then((response) => {
          let data = response.data.data;
          if (data) {
            context.commit("REMOVE_TRANSPARENCY", data)
          }
          resolve(response);
        }).catch((err) => {
        reject(err);
      })
    })
  },
  exportTransparencies() {
    return new Promise((resolve, reject) => {
      axios.get(`/admin/transparencies/csv`)
        .then(response => {
          resolve(response);
        })
        .catch(err => {
          reject(err);
        })
    })
  },
  deleteFavorite() {
    return new Promise((resolve, reject) => {
      axios.post(`/passes`)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        })
    });
  },
  createPass(context, pass) {
    return new Promise((resolve, reject) => {
      axios.post(`/passes`, pass)
        .then((response) => {
          resolve(response);
          context.dispatch("getPasses");
          context.commit('SET_ACTIVE_PASS', response.data.data)
        })
        .catch((err) => {
          reject(err);
        });
    });

  },
  changePassHistoryTime(context, data) {
    return new Promise((resolve, reject) => {
      axios.put(`/admin/passes-histories/${data.passID}/edit-time`, data.newTime)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  getStudentPassHistory(context, userID) {
    return new Promise((resolve, reject) => {
      axios.get(`/admin/passes/history/${userID}`)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  getStudentPassActives(context) {
    return new Promise((resolve, reject) => {
      axios.get(`/passes/actives`)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  getStudentPassHistoryAsStudent(context) {
    return new Promise((resolve, reject) => {
      axios.get(`/passes/history`)
        .then(response => {
          resolve(response);
          if(response.data.data){
            context.commit('SET_STUDENT_PASS_HISTORIES', response.data.data);
          }
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  getStudentActivePass(context) {
    return new Promise((resolve, reject) => {
      axios.get(`/passes/current`)
        .then(response => {

          let data = response.data.data;
          if (data && data.id) {
            context.commit('SET_ACTIVE_PASS', data);
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  cancelActivePass(context, pass) {
    return new Promise((resolve, reject) => {
      axios.put(`/passes/${pass}/cancel`)
        .then(response => {
          resolve(response);
          context.commit('SET_ACTIVE_PASS', response.data.data);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  cancelPass(context, pass) {
    return new Promise((resolve, reject) => {
      axios.put(`/admin/passes/${pass}/cancel`)
        .then(response => {
          let data = response.data.data;
          if(data){
            context.commit('dashboard/UPDATE_PASS', data, {root: true});
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  acknowledgeStudentAppointmentPass(context, pass) {
    return new Promise((resolve, reject) => {
      axios.put(`/passes/appointments/${pass}/acknowledge`)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  setPassStatus(context, pass) {
    return new Promise((resolve, reject) => {
      axios.put(`/admin/passes/${pass.id}`, pass.data)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  updatePass(context, {
    id,
    data
  }) {
    return new Promise((resolve, reject) => {
      axios.put(`/passes/${id}`, data)
        .then(response => {
          context.commit('SET_ACTIVE_PASS', response.data.data);
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  autoApprovePass(context, {id, action}) {
    return new Promise((resolve, reject) => {
      axios.put(`/passes/${id}/auto-approve`, {action})
        .then(response => {
          context.commit('SET_ACTIVE_PASS', response.data.data);
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  autoCheckIn(context, {id, roomPin}) {
    return new Promise((resolve, reject) => {
      axios.put(`/passes/${id}/auto-checkin`, {room_pin:roomPin})
        .then(response => {
          context.commit('SET_ACTIVE_PASS', response.data.data);
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
