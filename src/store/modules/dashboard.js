import axios from 'axios';
import rateLimit from 'axios-rate-limit';

let ai  = {
  baseURL: process.env.VUE_APP_API,
  headers:{
    common:{}
  }
}
ai.headers.common['Authorization'] = `Bearer ${localStorage.getItem('accessToken')}`;
ai.headers.common['Access-Control-Allow-Origin'] = '*';
ai.headers.common['Accept'] = 'application/json, text/plain, */*';

const http = rateLimit(axios.create(ai), { maxRequests: 1, perMilliseconds: 7000, maxRPS: 2 })
http.setRateLimitOptions({ maxRequests: 1, perMilliseconds: 7000 }) // same options as constructor
import api from './axiousWithCache';

const state = {
  stats: null,
  unavailable: null,
  passes: [],
  autoPassesRooms: null,
  staffSchedules: null,
  tabCounters: null,
};

const getters = {
  stats(state) {
    return state.stats;
  },
  passes(state) {
    return state.passes;
  },
  autoPassesRooms(state){
    return state.autoPassesRooms;
  },
  staffSchedules(state){
    return state.staffSchedules;
  },
  unavailable(state){
    return state.unavailable;
  },
  formListAutoPassesRooms(state){
    if(state.autoPassesRooms){
      return state.autoPassesRooms.map(room => {
        return {value: {id: room.room.id, type: 'App\\Models\\Room'}, label: room.room.name};
      });
    }
  },
};

const mutations = {
  SET_STATS(state, statsData) {
    state.stats = statsData;
  },
  SET_SPECIFIC_STAT(state, stat) {
    state.stats[stat.key] = stat.value;
  },
  SET_UNAVAILABLE(state, unavailable){
    state.unavailable = unavailable;
  },
  SET_PASSES(state, passesCollection){
    state.passes = passesCollection;
  },
  SET_AUTO_PASSES_ROOMS(state, data){
    state.autoPassesRooms = data;
  },
  SET_STAFF_SCHEDULES(state, data){
    state.staffSchedules = data;
  },
  UPDATE_AUTO_PASS_LIMIT(state, passLimit){
    if(state.staffSchedules){
      let newCollection = state.staffSchedules.map((limit) => {
        if(limit.room_id == passLimit.room_id){
          return passLimit;
        }
        return limit;
      });
      state.staffSchedules = newCollection;
    }
  },
  UPDATE_PASS(state, passObj){
    let aval = state.passes.filter(pass => pass.id === passObj.id);
      if(aval && aval[0]){
      let newCollection = state.passes.map((pass) => {
        if(pass.id === passObj.id){
          return passObj;
        }
        return pass;
      });
      state.passes = newCollection;
    }else{
      if(state.passes.length){
        state.passes.push(passObj);
      }else{
        state.passes = [passObj];
      }
    }
  },
  DECREMENT_STAT(state, type){
    if (state.stats[type] > 0) {
      state.stats[type]--;
    }
  },
  INCREMENT_STAT(state, type){
    state.stats[type]++;
  },
  PUSH_PASS(state, pass){
    if(state.passes.length){
      state.passes.push(pass);
    }else{
      state.passes = [pass];
    }
  },
  UPDATE_PASS_COMMENTS(state, passObj){
    let newCollection = state.passes.map((pass) => {
      if(pass.id === passObj.id){
        pass.comments = passObj.comments;
      }
      return pass;
    });
    state.passes = newCollection;
  },
  PUSH_PASSES(state, passesCollection){
    if(state.passes && state.passes.length){
      state.passes = state.passes.concat(passesCollection);
    }else{
      state.passes = passesCollection;
    }
  },
  SET_TAB_COUNTERS(state, counters){
    state.tabCounters = counters;
  }
};

const actions = {
  getDashboard(context) {
    return new Promise((resolve, reject) => {
      axios.get("/admin/users/dashboard").then(response => {
        resolve(response);
        let data = response.data.data;
        if (data.students && data.students.length) {
          context.commit('users/SET_USERS', data.students, { root: true });
        }
        if (data.hasOwnProperty('stats')) {
          context.commit('SET_STATS', data.stats);
        }
        if (data.hasOwnProperty('unavailable')) {
          context.commit('SET_UNAVAILABLE', data.unavailable);
        }
        if(data.hasOwnProperty('autoPassesRooms')){
          context.commit('SET_AUTO_PASSES_ROOMS', data.autoPassesRooms);
        }
        if(data.hasOwnProperty('staffSchedules')){
          context.commit('SET_STAFF_SCHEDULES', data.staffSchedules);
        }
        if(data.hasOwnProperty('tabCounters')){
          context.commit('SET_TAB_COUNTERS', data.tabCounters);
        }
      }).catch(error => {
        reject(error);
      })
    })
  },
  getDashboardPassesData(context, filterData) {
    return new Promise((resolve, reject) => {
      axios.get("/admin/users/dashboard/filter", {
        params: filterData
      })
      .then(response => {
        resolve(response);
      }).catch(error => {
        reject(error);
      })
    });
  },
  getDashboardPassesDataSocket(context, filterData) {
    return new Promise((resolve, reject) => {
      http.get("/admin/users/dashboard/filter", {
        params: {...filterData, fromSocket: true}
      })
      .then(response => {
        resolve(response);
      }).catch(error => {
        reject(error);
      })
    });
  },
  getDashboardPassesExport(context, filterData) {
    return new Promise((resolve, reject) => {
      axios.get("/admin/users/dashboard/export/csv", {
        params: filterData
      })
      .then(response => {
        resolve(response);
      }).catch(error => {
        reject(error);
      })
    });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
