import axios from 'axios';

const state = {
  user: null,
  attempts: null,
  isUserInTransparency: false,
};

const getters = {
  user(state) {
    return state.user;
  },
  formListUser(state){
    return [{
      value: {
        id: state.user.id,
        type: 'App\\Models\\User',
        passesToday: state.user.passes_for_today_count ? parseInt(state.user.passes_for_today_count) : 0
      },
      label: state.user.first_name + ' ' + state.user.last_name
    }];
  },
  getUserRole(state){
    if(state.user && state.user.role_id){
      switch (state.user.role_id) {
        case 1:
          return 'student';
        case 2:
          return 'admin';
        case 3:
          return 'teacher';
        case 4:
          return 'staff';
        case 5:
          return 'superadmin';
        default:
          return null;
      }
    }
  },
  isStudent(state) {
    return state.user.role_id === 1;
  },
  isAuthenticated(state){
    return !!state.user;
  },
  isUserAvailable(state){
    return state.user && state.user.is_available;
  },
  isMuted(state){
    return !state.user.audio_enabled;
  },
  attempts(state){
    return state.attempts;
  },
  pinAttemptsCount(state) {
    return state.user && state.user.pin_attempts_count;
  },
  isUserInTransparency(state) {
    return state.isUserInTransparency;
  }
};

const mutations = {
  SET_USER(state, userData) {
    state.user = userData;
  },
  TOGGLE_USER_AUDIO(state){
    state.user.audio_enabled = !state.user.audio_enabled;
  },
  UPDATE_USER(state, userData){
    state.user = userData ? Object.assign(state.user, userData) : state.user;
  },
  SET_USER_UNAVAILABLE(state, status){
    state.user.is_available = status;
  },
  SET_ATTEMPTS(state, attempts){
    state.attempts = attempts;
  },
  SET_USER_IS_LOCKED(state, data) {
    state.user.is_locked = data;
  },
  RESET_PIN_ATTEMPTS_COUNT(state) {
    state.user.pin_attempts_count = 0;
  },
  SET_USER_TRANSPARENCY(state, transparencyStatus){
    state.isUserInTransparency = transparencyStatus;
  }
};

const actions = {
  login(context, user) {
    return new Promise((resolve, reject) => {
      axios.post("/users/login", user)
      .then(response => {
        resolve(response);
      })
      .catch(error => {
        reject(error.response.data);
      })
    });
  },
  resetPassword(context, user) {
    return new Promise((resolve, reject) => {
      axios.post("/users/password/reset", user)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error.response.data);
        })
    });
  },
  getUserDetails(context) {
    return new Promise((resolve, reject) => {
      axios.get("/users/me")
      .then(response => {
        const user = response.data.data;
        if(user && user.created_at){
          context.commit('SET_USER', user);
        }
        if(response.data.hasOwnProperty('transparency_status')){
          context.commit('SET_USER_TRANSPARENCY', response.data.transparency_status);
        }
        resolve();
      })
      .catch(error => {
        reject(error);
      })
    })
  },
  unlock(context,pin){
    return new Promise((resolve, reject) => {
      axios.post("/users/unlock",{teacher_pin:pin})
      .then(response => {
        window.vueRouter.push('/');
        resolve();
      })
      .catch(error => {
        reject(error);
      })
    })
  },
  getAttempts(context){
    return new Promise((resolve, reject) => {
      axios.get("/users/unlock")
      .then(response => {
        context.commit('SET_ATTEMPTS', response.data.data);
      })
      .catch(error => {
        reject(error);
      })
    })
  },
  updateKioskPass(context, password){
    return new Promise((resolve, reject) => {
      axios.put(`/users/me`, {kiosk_password: password})
      .then((response) => {
        resolve(response);
      })
      .catch((err) => {
        reject(err);
      });
    });
  },
  logOut(context) {
    axios.post(`/users/logout`)
    .then((response) => {
      context.dispatch('unauthorize');
      window.location.replace('/login');
    })
    .catch((err) => {
      console.log(err);
    });
  },
  unauthorizeIfUserIsLogged({getters, dispatch}){
    if(getters.isAuthenticated){
      axios.post(`/users/logout`)
      .then((response) => {
        dispatch('unauthorize');
      })
      .catch((err) => {
        console.log(err);
      });
    }
  },
  saveToken(context, token){
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    localStorage.setItem('accessToken', token);
  },
  unauthorize(){
    localStorage.removeItem('accessToken');
    axios.defaults.headers.common['Authorization'] = ``;
  },
  externalLogin(context, data){
    return new Promise((resolve, reject) => {
      axios.post(`/login/oauth`, data)
      .then(response => {
        resolve(response);
      })
      .catch(error => {
        reject(error.response.data);
      })
    });
  },
  updateUserProfile(context, user){
    return new Promise((resolve, reject) => {
      axios.post(`/admin/users/profile`, user)
      .then((response) => {
        context.commit('UPDATE_USER', user);
        resolve(response);
      })
      .catch((err) => {
        reject(err);
      });
    });
  },
  deleteUserAvatar(){
    return new Promise((resolve, reject) => {
      axios.post(`/admin/users/delete/avatar`)
      .then((response) => {
        resolve(response);
      })
      .catch((err) => {
        reject(err);
      });
    });
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
