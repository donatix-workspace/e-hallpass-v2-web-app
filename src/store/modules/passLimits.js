import axios from 'axios';

const state = {
    passLimits: null,
    editablePassLimit: null
};

const getters = {
    passLimits(state) {
        return state.passLimits;
    },
    editablePassLimit(state){
        return state.editablePassLimit;
    }
};

const mutations = {
  SET_PASS_LIMITS(state, passLimits) {
    state.passLimits = passLimits;
  },
  PUSH_PASS_LIMITS(state, passLimits){
    if(state.passLimits && state.passLimits.length){
        state.passLimits = state.passLimits.concat(passLimits);
      }else{
        state.passLimits = passLimits;
      }
  },
  SET_EDITABLE_PASS_LIMITS(state, editablePassLimit) {
    state.editablePassLimit = editablePassLimit;
  },
  DELETE_PASS_LIMIT(state, passID){
    state.passLimits = state.passLimits.filter(el => el.id != passID)
  },
  UPDATE_PASS_LIMIT(state, passObj){
    let newCollection = state.passLimits.map((pass) => {
      if(pass.id === passObj.id){
        return passObj;
      }
      return pass;
    });
    state.passLimits = newCollection;
  },
  PUSH_PASS_LIMIT(state, passLimit){
    if(state.passLimits && state.passLimits.length){
      state.passLimits.push(passLimit);
    }else{
      state.passLimits = [passLimit];
    }
  }
};

const actions = {
    getPassLimits(context, params){
        return new Promise((resolve, reject) => {
            axios.get(`/admin/pass-limits`, {
                params
            })
            .then((response) => {
                resolve(response);
            })
            .catch((err) => {
                reject(err);
            });
        })
    },
    getCSVpassLimitsExport(context){
      return new Promise((resolve, reject) => {
          axios.get(`/admin/pass-limits/export/csv`)
          .then((response) => {
            resolve(response);
          })
          .catch((err) => {
            reject(err);
          });
      })
    },
    createPassLimit(context, passLimit) {
        return new Promise((resolve, reject) => {
            axios.post(`/admin/pass-limits`, passLimit)
                .then((response) => {
                  resolve(response);
                })
                .catch((err) => {
                    reject(err);
                });
            })
    },
    deletePassLimit(context, id){
        return new Promise((resolve, reject) => {
            axios.delete(`/admin/pass-limits/${id}`)
            .then((response) => {
                let data = response.data.data;
                if(data){
                    context.commit('DELETE_PASS_LIMIT', data.id);
                }
                resolve(response);
            })
            .catch((err) => {
                reject(err);
            });
        })
    },
    deletePassLimits(context, ids){
      return new Promise((resolve, reject) => {
        axios.post(`/admin/pass-limits/delete/bulk`, {pass_limit_ids:ids})
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
      })
    },
    updatePassLimit(context, data){
        return new Promise((resolve, reject) => {
            axios.put(`/admin/pass-limits/${data.id}`, data.params)
            .then((response) => {
                let data = response.data.data;
                if(data){
                    context.commit('UPDATE_PASS_LIMIT', data);
                }
                resolve(response);
            })
            .catch((err) => {
                reject(err);
            });
        })
    },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
