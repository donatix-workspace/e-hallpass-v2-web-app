import axios from 'axios';

const state = {
  sidebarShow: 'responsive',
  sidebarMinimize: false,
  asideShow: false,
  darkMode: false,
}

const mutations = {
  toggleSidebarDesktop(state) {
    const sidebarOpened = [true, 'responsive'].includes(state.sidebarShow)
    state.sidebarShow = sidebarOpened ? false : 'responsive'
  },
  toggleSidebarMobile(state) {
    const sidebarClosed = [false, 'responsive'].includes(state.sidebarShow)
    state.sidebarShow = sidebarClosed ? true : 'responsive'
  },
  set(state, [variable, value]) {
    state[variable] = value
  },
  toggle(state, variable) {
    state[variable] = !state[variable]
  },
}

const getters = {}
const actions = {
  toggleNotificationSounds(context, role_id) {
    if (role_id == 1) {
      axios.put(`/users/me/audio`)
      .then((response) => {
        context.commit('authentication/TOGGLE_USER_AUDIO', null, {
          root: true
        });
      })
      .catch((err) => {
        console.log(err);
      });
    } else {
      axios.post(`/admin/users/profile/audio`)
        .then((response) => {
          context.commit('authentication/TOGGLE_USER_AUDIO', null, {
            root: true
          });
        })
        .catch((err) => {
          console.log(err);
        });
    }

  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
