import axios from 'axios';

const state = {
    activePassLimits: null
};

const getters = {
    activePassLimits(state){
        return state.activePassLimits;
    }
};

const mutations = {
    SET_ACTIVE_PASS_LIMITS(state, data){
       state.activePassLimits = data;
    }
};

const actions = {
    createActivePassLimit(context, passLimit){
        return new Promise((resolve, reject) => {
            axios.put(`/admin/active-pass-limits`, passLimit)
            .then((response) => {
                context.commit('SET_ACTIVE_PASS_LIMITS', passLimit);
                resolve(response);
            })
            .catch((err) => {
                reject(err);
            });
        })
    },
    getActivePassLimits(context){
        return new Promise((resolve, reject) => {
            axios.get(`/admin/active-pass-limits`)
            .then((response) => {
                let data = response.data.data;
                if(data){
                    context.commit('SET_ACTIVE_PASS_LIMITS', data);
                }
                resolve(response);
            })
            .catch((err) => {
                reject(err);
            });
        })
    }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
