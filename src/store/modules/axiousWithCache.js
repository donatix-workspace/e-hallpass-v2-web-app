import axios from 'axios';
import { setupCache } from 'axios-cache-adapter';

const cache = setupCache({
  maxAge: 1 * 60 * 1000
})
let ai  = {
  baseURL: process.env.VUE_APP_API,
  headers:{
    common:{}
  },
  adapter: cache.adapter
}
ai.headers.common['Authorization'] = `Bearer ${localStorage.getItem('accessToken')}`;
ai.headers.common['Access-Control-Allow-Origin'] = '*';
ai.headers.common['Accept'] = 'application/json, text/plain, */*';

const api = axios.create(ai);

export default api;
