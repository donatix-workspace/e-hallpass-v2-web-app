import axios from 'axios';
import {Promise} from "core-js";

const state = {
  rooms: [],
  editableRoom: null,
  userAssignRooms: null,
  autoPassLimits: null,
  roomCapacityLimits: null,
  editableRoomCapacityLimit: null,
  locationRestrictions: null
};

const getters = {
  rooms(state) {
    return state.rooms;
  },
  editableRoom(state) {
    return state.editableRoom;
  },
  editableRoomCapacityLimit(state) {
    return state.editableRoomCapacityLimit;
  },
  formSelectRooms(state) {
    if(state.rooms){
      return state.rooms.map(room => {
        return {value: room.id, label: room.name};
      });
    }
  },
  formListRooms(state){
    if(state.rooms){
      return state.rooms.map(room => {
        return {value: {id: room.id, type: 'App\\Models\\Room', isFavorite: room.favorites, icon: room.icon,comment_type: room.comment_type}, label: room.name};
      });
    }
  },
  userAssignRooms(state){
    return state.userAssignRooms;
  },
  formListUserAssignRooms(state){
    if(state.userAssignRooms){
      return state.userAssignRooms.map(room => {
        return {value: {id: room.room.id, type: 'App\\Models\\Room', isFavorite: room.room.favorites, icon: room.room.icon}, label: room.room.name};
      });
    }
  },
  autoPassLimits(state){
    return state.autoPassLimits;
  },
  roomCapacityLimits(state){
    return state.roomCapacityLimits;
  },
  locationRestrictions(state){
    return state.locationRestrictions;
  }
};

const mutations = {
  SET_ROOMS(state, roomsData) {
    state.rooms = roomsData;
  },
  PUSH_ROOMS(state, roomsCollection){
    if(state.rooms && state.rooms.length){
      state.rooms = state.rooms.concat(roomsCollection);
    }else{
      state.rooms = roomsCollection;
    }
  },
  SET_USER_ASSIGN_ROOMS(state, roomsData) {
    state.userAssignRooms = roomsData;
  },
  SET_AUTO_PASS_LIMITS(state, data){
    state.autoPassLimits = data;
  },
  DELETE_ASSIGN_ROOM(state, roomID){
    state.userAssignRooms = state.userAssignRooms.filter(el => el.room_id != roomID);
  },
  SET_EDITABLE_ROOM(state, room) {
    state.editableRoom = room;
  },
  SET_EDITABLE_ROOM_CAP_LIMIT(state, limit) {
    state.editableRoomCapacityLimit = limit;
  },
  SET_ROOM_CAPACITY_LIMITS(state, limits) {
    state.roomCapacityLimits = limits;
  },
  PUSH_ROOM_CAPACITY_LIMITS(state, limits){
    if(state.roomCapacityLimits && state.roomCapacityLimits.length){
      state.roomCapacityLimits = state.roomCapacityLimits.concat(limits);
    }else{
      state.roomCapacityLimits = limits;
    }
  },
  PUSH_ROOM_CAPACITY_LIMIT(state, passLimit){
    if(state.roomCapacityLimits && state.roomCapacityLimits.length){
      state.roomCapacityLimits.push(passLimit);
    }else{
      state.roomCapacityLimits = [passLimit];
    }
  },
  UPDATE_ROOM_CAPACITY_LIMIT(state, passLimit){
    if(state.roomCapacityLimits){
      let newCollection = state.roomCapacityLimits.map((pass) => {
        if(pass.id === passLimit.id){
          return passLimit;
        }
        return pass;
      });
      state.roomCapacityLimits = newCollection;
    }
  },
  DELETE_ROOM_CAPACITY_LIMIT(state, limitID){
    state.roomCapacityLimits = state.roomCapacityLimits.filter(el => el.id != limitID)
  },
  SET_LOCATION_RESTRICTIONS(state, restrictions){
    state.locationRestrictions = restrictions;
  },
  PUSH_LOCATION_RESTRICTIONS(state, restrictions){
    if(state.locationRestrictions && state.locationRestrictions.length){
      state.locationRestrictions = state.locationRestrictions.concat(restrictions);
    }else{
      state.locationRestrictions = restrictions;
    }
  },
  PUSH_LOCATION_RESTRICTION(state, restriction){
    if(state.locationRestrictions && state.locationRestrictions.length){
      state.locationRestrictions.push(restriction);
    }else{
      state.locationRestrictions = [restriction];
    }
  },
  DELETE_LOCATION_RESTRICTION(state, id){
    state.locationRestrictions = state.locationRestrictions.filter(el => el.id != id)
  },
  UPDATE_LOCATION_RESTRICTION(state, restriction){
    let newCollection = state.locationRestrictions.map((el) => {
      if(el.id === restriction.id){
        return restriction;
      }
      return el;
    });
    state.locationRestrictions = newCollection;
  },
};

const actions = {
  getRooms(context, filterData) {
    return new Promise((resolve, reject) => {
      axios.get("/admin/rooms", {
        params: filterData
      },)
        .then(response => {
          resolve(response);
        })
        .catch(error => {

          reject(error);
        })
    });
  },
  toggleAPTRequest(context, data) {
    return new Promise((resolve, reject) => {
      axios.post(`/admin/rooms/${data.id}/toggleAPT`)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        })
    })
  },
  getUserAssignRooms(context) {
    return new Promise((resolve, reject) => {
      axios.get("/admin/rooms/assign")
        .then(response => {
          let data = response.data.data;
          if(data && data.assigned_rooms){
            context.commit('SET_USER_ASSIGN_ROOMS', data.assigned_rooms);
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  assignRoomsToUser(context, rooms) {
    return new Promise((resolve, reject) => {
      axios.post("/admin/rooms/assign", rooms)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  deleteAssingRooms(context, room_id) {
    return new Promise((resolve, reject) => {
      axios.delete("/admin/rooms/assign", {
        params:{
          room_ids: [room_id]
        }
        })
        .then(response => {
          context.commit('DELETE_ASSIGN_ROOM', room_id);
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  createRoom(context, room) {
    return new Promise((resolve, reject) => {
      axios.post(`/admin/rooms`, room)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        })
    })
  },
  updateRoom(context, room) {
    return new Promise((resolve, reject) => {
      axios.put(`/admin/rooms/${room.roomId}`, room.data)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  updateRoomIcon(context, room) {
    return new Promise((resolve, reject) => {
      axios.put(`/admin/rooms/${room.roomId}/icon`, room.data)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  importRooms(context, data) {
    return new Promise((resolve, reject) => {
      axios.post(`/admin/rooms/import/csv`, data)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        })
    })
  },
  createRoomRestriction(context, roomRestriction){
    return new Promise((resolve, reject) => {
      axios.post(`/admin/location-restrictions`, roomRestriction)
      .then((response) => {
          resolve(response);
      })
      .catch((err) => {
          reject(err);
      });
    })
  },
  getLocationRestrictions(context, params){
    return new Promise((resolve, reject) => {
      axios.get(`/admin/location-restrictions`, {
        params,
      })
      .then((response) => {
        resolve(response);
      })
      .catch((err) => {
        reject(err);
      });
  })
  },
  getCSVroomCapacityLimitsExport(context, params){
    return new Promise((resolve, reject) => {
        axios.get(`/admin/capacity-limits/export/csv`,
        {
          params,
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  getCSVlocationRestrictionsExport(context, params){
    return new Promise((resolve, reject) => {
        axios.get(`/admin/location-restrictions/export/csv`, {
          params,
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  deleteLocationRestriction(context, id){
    return new Promise((resolve, reject) => {
      axios.delete(`/admin/location-restrictions/${id}`)
      .then((response) => {
        context.commit('DELETE_LOCATION_RESTRICTION', id);
        resolve(response);
      })
      .catch((err) => {
        reject(err);
      });
  })
  },
  deleteLocationRestrictions(context, restrictionsIds){
    return new Promise((resolve, reject) => {
      axios.post(`/admin/location-restrictions/delete/bulk`, {room_restriction_ids:restrictionsIds})
      .then((response) => {
        resolve(response);
      })
      .catch((err) => {
        reject(err);
      });
  })
  },
  toggleLocationRestriction(context, id){
    return new Promise((resolve, reject) => {
      axios.put(`/admin/location-restrictions/${id}/status`)
      .then((response) => {
        resolve(response);
      })
      .catch((err) => {
        reject(err);
      });
  })
  },
  getRoomCapacityLimit(context, params){
    return new Promise((resolve, reject) => {
        axios.get(`/admin/capacity-limits`, {
          params,
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  createRoomCapacityLimit(context, locationLimit){
    return new Promise((resolve, reject) => {
        axios.post(`/admin/capacity-limits`, locationLimit)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  deleteRoomCapacityLimit(context, id){
    return new Promise((resolve, reject) => {
        axios.delete(`/admin/capacity-limits/${id}`)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  updateRoomCapacityLimit(context, locationLimit){
    return new Promise((resolve, reject) => {
        axios.put(`/admin/capacity-limits/${locationLimit.id}`, {
          limit: locationLimit.limit
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  addAutoPassLimit(context, limitData){
    return new Promise((resolve, reject) => {
        axios.post(`/admin/rooms/autopass/preference`, limitData)
        .then((response) => {
            resolve(response);
        })
        .catch((err) => {
            reject(err);
        });
    })
  },
  getAutoPassLimit(context){
    return new Promise((resolve, reject) => {
        axios.get(`/admin/rooms/autopass`)
        .then((response) => {
            let data = response.data.data;
            context.commit('SET_AUTO_PASS_LIMITS', data);
            resolve(response);
        })
        .catch((err) => {
            reject(err);
        });
    })
  },
  setAutoPassLimit(context, data){
    return new Promise((resolve, reject) => {
      axios.post(`/admin/rooms/autopass`, data)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  exportRooms(context, data) {
    return new Promise((resolve, reject) => {
      axios.get(`/admin/rooms/export/csv`, data)
        .then(response => {
          resolve(response);
        })
        .catch(err => {
          reject(err);
        })
    })
  },
  exportExampleCSV(context) {
    return new Promise((resolve, reject) => {
      axios.get(`/admin/rooms/export/csv_template`)
        .then(response => {
          resolve(response);
        })
        .catch(err => {
          reject(err);
        })
    })
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
