import axios from 'axios';

const state = {
  appointmentPasses: null,
  editablePass: null,
  todaysPassesNotf:null,
  appPassRefresh:null
};

const getters = {
  appointmentPasses(state) {
    return state.appointmentPasses;
  },
  editablePass(state) {
    return state.editablePass;
  },
  todaysPassesNotf(state){
    return state.todaysPassesNotf;
  },
  appPassRefresh(state){
    return state.appPassRefresh;
  }
};

const mutations = {
  SET_APPOINTMENT_PASSES(state, passesCollection) {
    state.appointmentPasses = passesCollection;
  },
  PUSH_APPOINTMENT_PASSES(state, passesCollection) {
    if(state.appointmentPasses && state.appointmentPasses.length){
      state.appointmentPasses = state.appointmentPasses.concat(passesCollection);
    }else{
      state.appointmentPasses = passesCollection;
    }
  },
  SET_EDITABLE_PASS(state, pass) {
    state.editablePass = pass;
  },
  SET_APPOINTMENT_PASSES_COUNTER(state,counter){
    state.todaysPassesNotf = counter;
  },
  UPDATE_APP_PASS(state, passObj){
    let newCollection = state.appointmentPasses.map((pass) => {
      if(pass.id === passObj.id){
        return passObj;
      }
      return pass;
    });
    state.appointmentPasses = newCollection;
  },
  UPDATE_APP_PASS_COMMENTS(state, passObj){
    let newCollection = state.appointmentPasses.map((pass) => {
      if(pass.id === passObj.id){
        pass.comments = passObj.comments;
      }
      return pass;
    });
    state.appointmentPasses = newCollection;
  },
  PUSH_APP_PASS(state, pass){
    state.appointmentPasses.push(pass);
  },
  DELETE_APP_PASS(state, passID){
    state.appointmentPasses = state.appointmentPasses.filter(el => el.id != passID);
  },
  SOCKET_UPDATE_APP_PASSES(state, passObj){
    let newPass = null;
    let newCollection = state.appointmentPasses.map((pass) => {
      if(pass.id === passObj.id){
        return passObj;
      }
      newPass = passObj;
      return pass;
    });
    if(newPass){
      newCollection.push(newPass);
    }
    state.appointmentPasses = newCollection;
  },
  SOCKET_REFRESH_APP_PASSES(state,update){
    state.appPassRefresh = update;
  }

};

const actions = {
  createAppointmentPass(context, appointments) {
    return new Promise((resolve, reject) => {
      axios.post(`/admin/passes/appointments`, appointments)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  createStudentAppointmentPass(context, appointments) {
    return new Promise((resolve, reject) => {
      axios.post(`/passes/appointments/request`, appointments)
        .then((response) => {
          resolve(response);

        })
        .catch((err) => {
          reject(err);

        });
    })
  },
  getAppointmentPassesAsStudent(context, filterData) {
    return new Promise((resolve, reject) => {
      axios.get(`/passes/appointments`, {
          params: filterData
        })
        .then((response) => {
          let data = response.data.data;
          if (data) {
            context.commit('SET_APPOINTMENT_PASSES', response.data.data);
          }
          let meta = response.data.meta
          if(meta){
            context.commit('SET_APPOINTMENT_PASSES_COUNTER', response.data.meta.today_appointments_count);
          }
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  getAppointmentPasses(context, filterData) {
    return new Promise((resolve, reject) => {
      axios.get(`/admin/passes/appointments/filter`, {
          params: filterData
        })
        .then((response) => {
          let meta = response.data.meta
          if(meta){
            context.commit('SET_APPOINTMENT_PASSES_COUNTER', response.data.meta.today_appointments_count);
          }
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  getAppRecurrencePasses(context, filterData) {
    return new Promise((resolve, reject) => {
      axios.get(`/admin/passes/appointments/recurrence`, {
          params: filterData
        })
        .then((response) => {
          let meta = response.data.meta
          if(meta){
            context.commit('SET_APPOINTMENT_PASSES_COUNTER', response.data.meta.today_appointments_count);
          }
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  getCSVAppointmentPasses(context, filterData) {
    return new Promise((resolve, reject) => {
      axios.get(`/admin/passes/appointments/csv`, {
          params: filterData
        })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  comfirmAppointmentPass(context, passID) {
    return new Promise((resolve, reject) => {
      axios.put(`/admin/passes/appointments/${passID}/confirm`)
        .then((response) => {
          let passObj = response.data.data;
          if(passObj && passObj.created_at){
            context.commit('UPDATE_APP_PASS', passObj);
          }
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  acknowledgeAppointmentPass(context, passID) {
    return new Promise((resolve, reject) => {
      axios.put(`/admin/passes/appointments/${passID}/acknowledge`)
        .then((response) => {
          let pass = response.data.data;
          if(pass){
            context.commit('UPDATE_APP_PASS', pass);
          }
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  cancelAppointmentPass(context, pass) {
    return new Promise((resolve, reject) => {
      const fromRecurrence = pass.fromRecurrence ? "?from_recurrence" : "";
      const passMessage = pass.message || null;
      axios.put(`/admin/passes/appointments/${pass.id}/cancel${fromRecurrence}`, passMessage)
        .then((response) => {
          let passObj = response.data.data;
          if(passObj && passObj.created_at){
            context.commit('UPDATE_APP_PASS', passObj);
          }
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  deleteRecurrenceAppointmentPass(context, pass) {
    return new Promise((resolve, reject) => {
      axios.delete(`/admin/passes/appointments/recurrence/${pass.id}`,{
        params:{
          action: pass.action
        }
      })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  deleteAppointmentPass(context, pass) {
    return new Promise((resolve, reject) => {
      axios.put(`/admin/passes/${pass.id}`)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  cancelAppointmentPassAsStudent(context, pass) {
    return new Promise((resolve, reject) => {
      axios.put(`/passes/appointments/${pass.id}/cancel`, pass.message)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  cancelRecurrenceAppointmentPass(context, pass) {
    return new Promise((resolve, reject) => {
      axios.put(`/admin/passes/appointments/recurrence/${pass.id}/cancel`,{action: pass.action})
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  editAppointmentPass(context, pass) {
    return new Promise((resolve, reject) => {
      axios.put(`/admin/passes/appointments/${pass.id}`, pass.data)
        .then((response) => {
          let passObj = response.data.data;
          if(passObj && passObj.created_at){
            context.commit('UPDATE_APP_PASS', passObj);
          }
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  editRecurrenceAppPass(context, pass) {
    return new Promise((resolve, reject) => {
      axios.put(`/admin/passes/appointments/recurrence/${pass.id}`, pass.data)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  acknowledgeAppointmentPassEmail(context, token) {
    return new Promise((resolve, reject) => {
      axios.get(`/passes/appointments/acknowledge-mail?token=${token}`)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  getInfoForExistApp(context, params) {
    return new Promise((resolve, reject) => {
      axios.get(`/admin/users/student-limit-info/${params.studentID}?apt_for_time=${params.date}`)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
