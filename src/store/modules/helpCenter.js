import axios from 'axios';
import api from './axiousWithCache';
const state = {
  content: null,
}

const mutations = {
  SET_CONTENT(state, content){
    state.content = content;
  }
}

const getters = {
    getHelpCenterContent(state, key){
      return state.content;
      // return state[key];
    }
}
const actions = {
  getHelpCenterContent(context,key){
    return new Promise((resolve, reject) => {
      api.get(`/static-content/help-center?key=${key}`)
        .then(response => {
            console.log(response);
            context.commit('SET_CONTENT',response.data.data)
            resolve(response);
        })
        .catch(error => {
          console.log(error);
          context.commit('SET_CONTENT','No Information or this key')
          reject(error);
        })
    });
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
