import axios from 'axios';
import {Promise} from "core-js";
import api from './axiousWithCache';
const state = {
  periods: [],
  editablePeriod: null,
};

const getters = {
  periods(state) {
    return state.periods;
  },
  editablePeriod(state) {
    return state.editablePeriod;
  },
  activePeriods(state){
    if(state.periods){
      return state.periods.filter(period => period.status);
    }
  },
  formListStudents(state, getters) {
    if (state.periods) {
      return getters.activePeriods.map(periods => {
        return {value: periods.id, label: periods.name};
      });
    }
  }
};

const mutations = {
  SET_PERIODS(state, periodsCollection) {
    state.periods = periodsCollection;
  },
  SET_EDITABLE_PERIOD(state, period) {
    state.editablePeriod = period;
  }
};

const actions = {
  getPeriods(context) {
    return new Promise((resolve, reject) => {
      api.get(`/admin/periods`)
        .then((response) => {
          let data = response.data.data;
          if (data && data.length) {
            context.commit('SET_PERIODS', data);
          }
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  createPeriod(context, period) {
    return new Promise((resolve, reject) => {
      axios.post("/admin/periods", period)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  updatePeriod(context, period) {
    return new Promise((resolve, reject) => {
      axios.put(`/admin/periods/${period.periodId}`, period.data)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  updatePosition(context, data) {
    return new Promise((resolve, reject) => {
      axios.put(`/admin/periods/order`, {periods: data})
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  deletePeriod(context, data) {
    return new Promise((resolve, reject) => {
      axios.delete(`/admin/periods/${data.periodId}`)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
