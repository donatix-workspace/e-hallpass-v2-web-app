import axios from 'axios';

const state = {
  summaryReports: [],
  formData: {},
};

const mutations = {
  SET_SUMMARY_REPORTS(state, summaryReportsCollection) {
    state.summaryReports = summaryReportsCollection;
  },
  SET_SUMMARY_REPORTS_FORM_DATA(state, summaryReportsFormData) {
    state.formData = summaryReportsFormData;
  },
  PUSH_SUMMARY_REPORTS(state, reportsCollection){
    if(state.summaryReports && state.summaryReports.length){
      state.summaryReports = state.summaryReports.concat(reportsCollection);
    }else{
      state.summaryReports = reportsCollection;
    }
  }
};

const getters = {
  summaryReports() {
    return state.summaryReports;
  },
  students() {
    return state.formData.students ? state.formData.students.filter(user => user.role_id === 1) : null;
  },
  staffUsers() {
    return state.formData.adults ? state.formData.adults.filter(user => user.role_id != 1) : null;
  },
  formListStaff(state, getters) {
    if (getters.staffUsers) {
      return getters.staffUsers.map(user => {
        return {
          value: user.id,
          label: user.first_name + ' ' + user.last_name
        };
      });
    }
  },
  formListStudents(state, getters) {
    return state.formData.students ? getters.students.map(user => {
      return {
        value: user.id,
        label: user.first_name + ' ' + user.last_name
      };
    }) : null;
  },
  formListRooms(state) {
    return state.formData.rooms ? state.formData.rooms.map(room => {
      return {
        value: room.id,
        label: room.name
      };
    }) : null;
  },
};

const actions = {
  getSummaryReportsFormData(context) {
    return new Promise((resolve, reject) => {
      axios.get(`/admin/reports/summary`)
        .then(response => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        })
    })
  },
  getSummaryReports(context, data) {
    return new Promise((resolve, reject) => {
      axios.get(`/admin/reports/summary/filter`, {
        params: data
      })
        .then(response => {
          resolve(response);
        })
        .catch(err => {
          reject(err);
        });
    })
  },
  getSummaryReportsResults(context, data) {
    return new Promise((resolve, reject) => {
      axios.get(`/admin/reports/summary/results`, {
        params: data
      })
        .then(response => {
          resolve(response);
        })
        .catch(err => {
          reject(err);
        });
    })
  },
  exportSummaryReports(context, data) {
    return new Promise((resolve, reject) => {
      axios.get(`/admin/reports/summary/filter`, {
        params: data
      })
        .then(response => {
          resolve(response);
        })
        .catch(err => {
          reject(err);
        })
    })
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
