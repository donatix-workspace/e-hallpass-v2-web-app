import axios from 'axios';
import {Promise} from "core-js";
import _ from 'underscore';

const state = {
  kiosks: [],
  active_kiosk: null,
  access_token: null,
  createNewPassData: null,
  activePass: null,
  passHistories: [],
};

const kioskInstance = axios.create({
  baseURL: process.env.VUE_APP_API,
  timeout: 1000,
});

const getters = {
  kiosks(state) {
    return state.kiosks;
  },
  activeKiosk(state) {
    return state.active_kiosk;
  },
  activePass(state) {
    return state.activePass;
  },
  accessToken(state) {
    return state.access_token;
  },
  createNewPassData(state){
    return state.createNewPassData;
  },
  passHistories(state){
    return state.passHistories;
  }
};

const mutations = {
  SET_KIOSKS(state, kiosksCollection) {
    state.kiosks = kiosksCollection;
  },
  SET_ACTIVE_KIOSK(state, kiosk) {
    state.active_kiosk = kiosk;
  },
  SET_ACTIVE_PASS(state, pass) {
    state.activePass = pass;
  },
  SET_KIOSK_ACCESS_TOKEN(state, accessToken) {
    state.access_token = accessToken;
    kioskInstance.defaults.headers.common['Authorization'] = `Bearer ${accessToken}`;
    localStorage.setItem('kioskAccessToken', accessToken);
  },
  UPDATE_KIOSK(state, newKiosk) {
    let oldKiosk = _.findIndex(state.kiosks, {id: newKiosk.id});
    state.kiosks.splice(oldKiosk, 1, newKiosk);
  },
  ADD_KIOSK(state, newKiosk) {
    state.kiosks.unshift(newKiosk);
  },
  DEACTIVATE_KIOSK(state, kiosk) {
    state.kiosks = state.kiosks.filter((oldKiosk) => oldKiosk.id !== kiosk.id);
  },
  SET_CREATE_NEW_PASS_DATA(state, data){
    state.createNewPassData = data;
  },
  SET_PASS_HISTORIES(state, collection){
    state.passHistories = collection;
  }
};

const actions = {
  getKiosks(context) {
    return new Promise((resolve, reject) => {
      axios.get("/kiosk")
        .then(response => {
          let data = response.data.data;
          if (data) {
            context.commit('SET_KIOSKS', data);
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  getPasses(context) {
    return new Promise((resolve, reject) => {
      kioskInstance.get("/kiosk/passes")
        .then(response => {
          let data = response.data.data;
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  createKiosk(context, kioskData) {
    return new Promise((resolve, reject) => {
      axios.post("/kiosk", kioskData)
        .then(response => {
          let data = response.data.data;
          if (data) {
            context.commit('ADD_KIOSK', data);
          }
          resolve(response);
        }).catch(error => {
        reject(error);
      })
    })
  },
  directLogin(context, data) {
    return new Promise((resolve, reject) => {
      kioskInstance.post("/kiosk/direct", data)
      .then(response => {
        resolve(response);
      })
      .catch(error => {
        reject(error);
      })
    })
  },
  userLogin(context, data) {
    return new Promise((resolve, reject) => {
      kioskInstance.post("/kiosk/login", data)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          console.log(error);
          reject(error);
      })
    })
  },
  logOut(){
    kioskInstance.post("/kiosk/logout")
    .then(response => {
    })
    .catch(error => {
      console.log(error);
    })
  },
  userLogout(context) {
    context.dispatch('logOut');
    context.commit('SET_KIOSK_ACCESS_TOKEN', null);
    context.commit('SET_ACTIVE_PASS', null);
    context.commit('SET_CREATE_NEW_PASS_DATA', null);
    kioskInstance.defaults.headers.common['Authorization'] = ``;
    localStorage.removeItem('kioskAccessToken');
  },
  createKioskPass(context, pass) {
    return new Promise((resolve, reject) => {
      kioskInstance.post(`/kiosk/passes`, pass)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  updatePass(context, {id, data}) {
    return new Promise((resolve, reject) => {
      kioskInstance.put(`/kiosk/passes/${id}`, data)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  deactivateKiosk(context, kiosk_id) {
    return new Promise((resolve, reject) => {
      axios.post("/kiosk/" + kiosk_id + "/deactivate")
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    })
  },
  sendCode(context, kiosk_id) {
    return new Promise((resolve, reject) => {
      axios.post("/kiosk/" + kiosk_id + "/email")
        .then(response => resolve(response))
        .catch(error => {
          console.log(error);
          reject(error);
        })
    })
  },
  regeneratePassword(context, kiosk_id) {
    return new Promise((resolve, reject) => {
      axios.post("/kiosk/" + kiosk_id + "/password/regenerate")
        .then(response => {
          let data = response.data.data;
          if (data) {
            context.commit('UPDATE_KIOSK', data)
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    })
  },
  searchByTypeAndQuery(context, params) {
    return new Promise((resolve, reject) => {
      kioskInstance.get(`/search`, {
        params,
      })
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  importKioskPWs(context, data) {
    return new Promise((resolve, reject) => {
      axios.post("/admin/users/imports/kiosk", data, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
      })
      .then(response => {
        resolve(response);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      })
    });
  },
  finalImportKioskPWs(context, data) {
    return new Promise((resolve, reject) => {
      axios.post(`/admin/users/imports/kiosk/${data.fileName}/final`, data.passwords)
      .then(response => {
        resolve(response);
      })
      .catch(error => {
        reject(error);
      })
    });
  },
  autoApprovePass(context, data){
    return new Promise((resolve, reject) => {
      kioskInstance.put(`/kiosk/passes/${data.passId}/auto-approve`, {action: data.action, kiosk_id: data.kiosk_id})
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  getFavorites(context, uuid){
    return new Promise((resolve, reject) => {
      kioskInstance.get(`/kiosk/favorites?cus_uuid=${uuid}`)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  getPassHistories(){
    return new Promise((resolve, reject) => {
      kioskInstance.get(`/kiosk/history`)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
