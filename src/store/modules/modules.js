import axios from "axios";

const state = {
};

const getters = {
};

const mutations = {
};

const actions = {
  updateAppointmentPassModule(context, data) {
    return new Promise((resolve, reject) => {
      axios.put(`/admin/modules/1`, data)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  updateKioskModule(context, data) {
    return new Promise((resolve, reject) => {
      axios.put(`/admin/modules/3`, data)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  toggleModule(context, data) {
    return new Promise((resolve, reject) => {
      axios.put(`/admin/modules/${data.id}/status`)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
