import axios from 'axios';
import {
  Promise
} from "core-js";

const state = {
  users: null,
  favorites: null,
  editableUser: null,
  editableSubstitute: null,
  stats: null,
};

const getters = {
  users(state) {
    return state.users;
  },
  students() {
    return state.users ? state.users.filter(user => user.role_id === 1) : null;
  },
  staffUsers() {
    return state.users ? state.users.filter(user => user.role_id != 1) : null;
  },
  substituteUsers() {
    return state.users ? state.users.filter(user => user.is_substitute) : null;
  },
  archivedUsers() {
    return state.users ? state.users.filter(user => user.current_role_in_school.pivot.archived_at != null) : null;
  },
  formListStudents(state, getters) {
    if (getters.students) {
      return getters.students.map(student => {
        return {
          value: {
            id: student.id,
            type: 'App\\Models\\User',
            passesToday: student.passes_for_today_count ? parseInt(student.passes_for_today_count) : 0
          },
          label: student.first_name + ' ' + student.last_name
        };
      });
    }
  },
  formListStaff(state, getters) {
    if (getters.staffUsers) {
      return getters.staffUsers.map(user => {
        return {
          value: {
            id: user.id,
            type: 'App\\Models\\User',
            avatar: user.avatar,
            comment_type: user.comment_type,
          },
          label: user.first_name + ' ' + user.last_name
        };
      });
    }
  },
  multiSelectStudentsList(state, getters) {
    if (getters.students) {
      return getters.students.map(student => {
        return {
          value: student.id,
          label: student.first_name + ' ' + student.last_name + ', ' + student.email
        };
      });
    }
  },
  fromFavorites(state) {
    return state.favorites;
  },
  editableUser(state) {
    return state.editableUser;
  },
  editableSubstitute(state) {
    return state.editableSubstitute;
  },
  stats(state) {
    return state.stats;
  }
};

const mutations = {
  SET_USERS(state, usersCollection) {
    state.users = usersCollection;
  },
  PUSH_USERS(state, usersCollection) {
    if (state.users && state.users.length) {
      state.users = state.users.concat(usersCollection);
    } else {
      state.users = usersCollection;
    }
  },
  REMOVE_USER(state, user) {
    state.users = state.users.filter(student => {
      return student.id !== user.id
    })
  },
  SET_FAVORITES(state, favoritesCollection) {
    state.favorites = favoritesCollection;
  },
  SET_EDITABLE_USER(state, user) {
    state.editableUser = user;
  },
  SET_EDITABLE_SUBSTITUTE(state, user) {
    state.editableSubstitute = user;
  },
  SET_STATS(state, statsData) {
    state.stats = statsData;
  }
};

const actions = {
  updateKioskPass(context, user) {
    return new Promise((resolve, reject) => {
      axios.put(`/users/${user.id}/edit`, user.password)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  getUsers(context, filterData) {
    return new Promise((resolve, reject) => {
      axios.get("/admin/users", {
        params: filterData
      },)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  getSubstituteUsers(context, filterData) {
    return new Promise((resolve, reject) => {
      axios.get("/admin/users/teachers/substitutes", {
        params: filterData
      },)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  createUser(context, user) {
    return new Promise((resolve, reject) => {
      axios.post("/admin/users", user)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  updateUser(context, user) {
    return new Promise((resolve, reject) => {
      axios.put(`/admin/users/${user.userId}/edit`, user.data)
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          reject(err);
        });
    })
  },
  createSubstituteTeacher(context, user) {
    return new Promise((resolve, reject) => {
      axios.post("/admin/users/teachers/create?is_substitute=true", user)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  updateSubstituteTeacher(context, data) {
    return new Promise((resolve, reject) => {
      axios.put(`/admin/users/teachers/substitute/${data.userId}?is_substitute=true`, data.data)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  archive(context, user) {
    return new Promise((resolve, reject) => {
      axios.post("/users/" + user.id + "/archive", user)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  unArchive(context, user) {
    return new Promise((resolve, reject) => {
      axios.put("/admin/users/" + user.id + "/unarchive")
        .then(response => {
          context.commit('REMOVE_USER', user);
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  substituteDeleteBulk(context, data) {
    return new Promise((resolve, reject) => {
      axios.post("/admin/users/teachers/substitute/delete/bulk", data)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  substituteBulkPin(context, data) {
    return new Promise((resolve, reject) => {
      axios.post("/admin/users/pin/create", data)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  deactivateSubstituteTeacher(context, data) {
    return new Promise((resolve, reject) => {
      axios.delete(`/admin/users/teachers/substitute/${data.userId}/delete`)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    })
  },
  activateSubstituteTeacher(context, data) {
    return new Promise((resolve, reject) => {
      axios.put(`/admin/users/teachers/substitute/${data.userId}/activate`)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    })
  },
  inviteBulk(context, data) {
    return new Promise((resolve, reject) => {
      axios.post(`/admin/users/invites/bulk`, data)
        .then(response => {
          resolve(response);
        }).catch(error => {
        reject(error);
      })
    })
  },
  archiveBulk(context, data) {
    return new Promise((resolve, reject) => {
      axios.put("/admin/users/archives/bulk/archive", data)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  unArchiveBulk(context, data) {
    return new Promise((resolve, reject) => {
      axios.put("/admin/users/archives/bulk/unarchive", data)
        .then(response => {
          context.dispatch('getUsers');
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  exportUsers(context, data) {
    return new Promise((resolve, reject) => {
      axios.get("/admin/users/export/csv", {
        params: {
          ...data,
          substitute: data.is_substitute ? data.is_substitute : false,
          archived: data.is_archived ? data.is_archived : false
        }
      })
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  },
  getStats(context) {
    return new Promise((resolve, reject) => {
      axios.get('/admin/users/statistics')
        .then(response => {
          let data = response.data.data.stats;
          if (data) {
            context.commit('SET_STATS', data);
          }
          resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    })
  },
    recreateUser(context, user) {
        return new Promise((resolve, reject) => {
            axios.put(`/admin/users/${user.id}/recreate`)
                .then((response) => {
                    resolve(response);
                })
                .catch((err) => {
                    reject(err);
                });
        })
    }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
