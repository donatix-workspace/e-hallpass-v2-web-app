import axios from 'axios';
import moment from 'moment-timezone';
import api from './axiousWithCache';

const state = {
  schools: null,
  activeSchool: null,
  activeSchoolModules: [],
};

const getters = {
  schools(state) {
    return state.schools;
  },
  activeSchool(state) {
    return state.activeSchool;
  },
  activeSchoolModules(state) {
    return state.activeSchoolModules.filter(school => school.pivot && school.pivot.status);
  },
  availableSchoolModules(state){
    return state.activeSchoolModules ? state.activeSchoolModules.map(module => module.name) : [];
  },
  activeModulesNames(state, getters){
    return getters.activeSchoolModules ? getters.activeSchoolModules.map(el => el.name) : [];
  },
  isActiveAppoinmentModule(state, getters){
    return getters.activeModulesNames.includes('Appointmentpass');
  },
  activeAppoinmentModuleOptions(state, getters){
    let apptModule = getters.activeSchoolModules ? getters.activeSchoolModules.filter(el => el.name === 'Appointmentpass') : [];
    return apptModule[0] ? JSON.parse(apptModule[0].pivot.option_json) : null;
  },
  activeKioskModuleOptions(state, getters){
    let kioskModule = getters.activeSchoolModules ? getters.activeSchoolModules.filter(el => el.name === 'Kiosk') : [];
    return kioskModule[0] ? JSON.parse(kioskModule[0].pivot.option_json) : null;
  },
  isActiveAutoCheckInModule(state, getters){
    return getters.activeModulesNames.includes('Auto Check-In');
  },
  isActiveKioskModule(state, getters){
    return getters.activeModulesNames.includes('Kiosk');
  },
  activeSchoolHasAppoitmentPasses(stat, getters) {
    let appointmentPass = getters.activeSchoolModules.find(module => module.name === 'Appointmentpass');
    if (!appointmentPass) {
      return false;
    }
    let appointmentPassOptions = JSON.parse(appointmentPass.pivot.option_json);
    return appointmentPassOptions.admin || appointmentPassOptions.location || appointmentPassOptions.staff ||
      appointmentPassOptions.teacher;
  },
  showStudentPhotos(state) {
    return state.activeSchool.show_photos;
  },
  activeSchoolAuthProviders(state) {
    return JSON.parse(state.activeSchool.auth_providers);
  },
};

const mutations = {
  SET_SCHOOLS(state, schoolsCollection) {
    state.schools = schoolsCollection;
  },
  SET_ACTIVE_SCHOOL(state, school) {
    state.activeSchool = school;
    if(school.school_domains){
      localStorage.setItem('school_domains', school.school_domains);
    }
    moment.tz.setDefault(school.timezone);
  },
  SET_ACTIVE_SCHOOL_MODULES(state, modules) {
    state.activeSchoolModules = modules.filter(el => el.status);
  }
};

const actions = {
  changeActiveSchool(context, school) {
    axios.put(`/admin/schools/${school.id}`)
      .then((response) => {
        context.commit('SET_ACTIVE_SCHOOL', school);
        window.location.replace('/dashboard');
      })
      .catch((err) => {
        console.log(err);
      });
  },
  getSchools(context) {
    api.get(`/users/me/schools`)
      .then((response) => {
        let data = response.data;
        if(data && data.data.length){
          let activeSchool = data.data.filter(school => school.active_school);
          context.commit('SET_SCHOOLS', data.data);
          if(activeSchool.length){
            context.commit('SET_ACTIVE_SCHOOL', activeSchool[0]);
            context.commit('SET_ACTIVE_SCHOOL_MODULES', activeSchool[0].modules);
          }
        }
      })
      .catch((err) => {
        console.log(err);
      });
  },
  // getActiveModules(context) {
  //   axios.get(`/admin/modules/active`)
  //     .then((response) => {
  //       console.log(response);
  //       let data = response.data.data;
  //       if(data){
  //         // context.commit('SET_ACTIVE_SCHOOL_MODULES', data);
  //       }
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });
  // },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
